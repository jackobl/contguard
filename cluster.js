'use strict';

let _ = require('lodash');

let start = new Date();

// Load JSON DATA
let vesselsData = require('./vessels.json').features;
let telemetriesData = require('./telemetries.json').features;

let numWorkers = require('os').cpus().length;
let cluster = require('cluster');

let worker;

if(cluster.isMaster) {
   
    console.log('Master cluster setting up ' + numWorkers + ' workers...');

    for(var i = 0; i < numWorkers; i++) {

        worker = cluster.fork();
        worker.send({
            part: i+1,
            from: 'master',
        });        
        worker.on('message', function(msg) {
            console.log('Worker to master: ', JSON.stringify(msg));
            this.kill();
        });        
    }

    cluster.on('online', function(worker) {
        console.log('Worker ' + worker.process.pid + ' is online');
           
    });
    

    cluster.on('exit', function(worker, code, signal) {
        console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
       // console.log('Starting a new worker');
       // cluster.fork();
    });
} else {
    
    process.on('message', function(message) {
        console.log('Process ' + process.pid + ' array part ' + message.part);

        console.info("Execution time: %dms", new Date() - start);
        
        process.send({id: process.pid, status: "done"});
    });  
       
}

