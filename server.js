'use strict';

let _ = require('lodash');

let start = new Date();

// Load JSON DATA
let vesselsData = require('./vessels.json').features;
let telemetriesData = require('./telemetries.json').features;

// Filter Valid GPS only
let result = _.filter(telemetriesData, function(o) { 
    let valid = "Valid";
    let str   = String(o.properties.GPS);        
    if (str.indexOf(valid) !== -1)
        return true;
    else
        return false;     
});

// intersection by time
result = _.intersectionWith(vesselsData, result, function(arrVal, othVal) {
    let deltaTime        = 900000;
    let timeVessels      = Date.parse(String(arrVal.properties.Time)); 
    let timeTelemetries  = Date.parse(String(othVal.properties.Received));
    let difference = Math.abs(timeVessels-timeTelemetries);
    // Time difference is less than 15 minutes
    if (difference < deltaTime) {
        // Longitude & Latitude are exactly the same (perfect match)
        if (arrVal.properties.Longitude === othVal.properties.Longitude && 
            arrVal.properties.Latitude  === othVal.properties.Latitude)        
            return true;
        else 
            return false;    
    } else 
        return false;    

});


console.info("Execution time: %dms", new Date() - start);
console.log(JSON.stringify(result));